﻿using System.Net;
using System.Security.Authentication;
using Serilog.Sinks.SystemConsole;

namespace AutoDailies
{
    internal class Program
    {
        private static Random _random = new ();

        private static readonly string _rewardActId = "e202102251931481";

        private static string[] _cookiesData =
        {
            
        };

        private static string[] _availableUserAgents =
        {
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 YaBrowser/20.9.0.933 Yowser/2.5 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36 OPR/74.0.3911.218",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0",
            "Mozilla/5.0 (Windows NT 6.1; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 ",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.54 Safari/537.36 Edg/101.0.1210.39 Agency/90.8.3027.28",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.54 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0"
        };

        public static string AccountInfoUrl =>
            "https://api-os-takumi.mihoyo.com/binding/api/getUserGameRolesByCookie?mhy_auth_required=true&mhy_presentation_style=fullscreen&utm_source=tools&lang=ru-ru&bbs_theme=dark&bbs_theme_device=1";

        public static string RewardInfoUrl =>
            $"https://hk4e-api-os.mihoyo.com/event/sol/info?act_id={_rewardActId}&mhy_auth_required=true&mhy_presentation_style=fullscreen&utm_source=tools&lang=ru-ru&bbs_theme=dark&bbs_theme_device=1";

        public static string ClaimRewardUrl =>
            $"https://hk4e-api-os.mihoyo.com/event/sol/sign?act_id={_rewardActId}&mhy_auth_required=true&mhy_presentation_style=fullscreen&utm_source=tools&lang=ru-ru&bbs_theme=dark&bbs_theme_device=1";

        public static string MonthRewardsUrl =>
            $"https://sg-hk4e-api.hoyolab.com/event/sol/home?lang=ru-ru&act_id={_rewardActId}&mhy_auth_required=true&mhy_presentation_style=fullscreen&utm_source=tools&bbs_theme=dark&bbs_theme_device=1";

        private static void Main(string[] args)
        {
            var accounts = new HashSet<MihoyoAccount>();

            while (true)
            {
                foreach ((Cookie ltoken, Cookie ltuid) in LoadCookies(_cookiesData))
                {
                    MihoyoAccount? account = accounts.FirstOrDefault(x => x.Ltuid == ltuid.Value); 

                    try
                    {
                        if (account == null)
                        {
                            account = new MihoyoAccount(ltoken, ltuid);
                            accounts.Add(account);
                        }

                        if (account.IsDailyClaimed == false)
                        {
                            if (account.TryClaimRewards(out MonthlyRewardData? reward) == false)
                            {
                                throw new AuthenticationException("Failed to authorize!");
                            }

                            Console.WriteLine($"Received an reward on account with ltuid {account.Ltuid}: {reward}");
                        }
                    }
                    catch (AuthenticationException)
                    {
                        Console.WriteLine($"Failed to authorize in Mihoyo Account with ltuid: {ltuid}\nMay be, cookies invalid or expired. Skipping...");

                        if (account != null && accounts.Contains(account))
                        {
                            accounts.Remove(account);
                        }
                    }
                    catch
                    {
                        Console.WriteLine($"An unexpected error occurred while trying to log into Mihoyo account with ltuid: {ltuid}!");
                    }

                    SleepRandomTime(120, 300);
                }

                SleepRandomTime(86400, 90800);
            }
        }

        public static string GetRandomUserAgent()
        {
            return _availableUserAgents[_random.Next(_availableUserAgents.Length)];
        }

        private static void SleepRandomTime(int minSleepSeconds, int maxSleepSeconds)
        {
            const int SecondsMillisecondsDifference = 1000;

            int secondsSleepTime = _random.Next(minSleepSeconds, maxSleepSeconds);
            Console.WriteLine("\n" +
                              $"It's time to sleep {secondsSleepTime} seconds!" +
                              "\n");

            int millisecondsSleepTime = secondsSleepTime * SecondsMillisecondsDifference;
            Thread.Sleep(millisecondsSleepTime);
        }

        private static HashSet<(Cookie ltoken, Cookie ltuid)> LoadCookies(string[] cookiesData)
        {
            HashSet<(Cookie, Cookie)> loadedCookies = new();

            foreach (string cookieData in cookiesData)
            {
                string[] cookies = cookieData.Split("; ");

                Cookie? ltoken = null;
                Cookie? ltuid = null;

                foreach (string cookie in cookies)
                {
                    string[] separatedCookie = cookie.Split("=");

                    if (separatedCookie[0] == "ltoken")
                    {
                        ltoken = new Cookie("ltoken", separatedCookie[1], "/", ".mihoyo.com");
                    }
                    else if (separatedCookie[0] == "ltuid")
                    {
                        ltuid = new Cookie("ltuid", separatedCookie[1], "/", ".mihoyo.com");
                    }
                }

                if (ltoken == null || ltuid == null)
                {
                    Console.WriteLine("Cookie is invalid!");
                    continue;
                }

                loadedCookies.Add((ltoken, ltuid));
            }

            return loadedCookies;
        }
    }
}