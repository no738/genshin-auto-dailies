﻿using Leaf.xNet;
using Newtonsoft.Json;
using System.Net;
using System.Security.Authentication;

namespace AutoDailies
{
    internal class MihoyoAccount
    {
        private readonly CookieCollection _cookies;
        private readonly string _selectedUserAgent = Program.GetRandomUserAgent();

        private const int DailyClaimedCacheExpirationMinutes = 30;
        private bool _isDailyClaimedCache;
        private DateTime _isDailyClaimedCacheTime = DateTime.MinValue;

        private MonthlyRewardsData _monthlyRewards;

        public MihoyoAccount(Cookie ltoken, Cookie ltuid)
        {
            _cookies = new CookieCollection { ltoken, ltuid };

            Ltuid = ltuid.Value;

            if (TryGetGameAccountsInfo(_cookies, out IReadOnlyList<GameAccountData>? accounts) == false)
            {
                throw new AuthenticationException("Failed to authorize!");
            }

            Console.WriteLine($"Successful authorization in Mihoyo account [Ltuid {Ltuid}]. There are {accounts.Count} Genshin Impact accounts:\n");
            Console.WriteLine(string.Join("\n", accounts));
            Console.WriteLine($"Is daily check-in already claimed: {IsDailyClaimed}");

            if (TryGetMothlyRewards(_cookies) == false)
            {
                throw new AuthenticationException("Failed to authorize!");
            }
        }

        public string Ltuid { get; }

        public bool IsDailyClaimed
        {
            get
            {
                if ((DateTime.Now - _isDailyClaimedCacheTime).TotalMinutes > DailyClaimedCacheExpirationMinutes)
                {
                    if (TryGetCurrentRewardsStatus(_cookies, out RewardsData? rewardStatus))
                    {
                        _isDailyClaimedCacheTime = DateTime.Now;
                        _isDailyClaimedCache = rewardStatus!.IsClaimed;
                    }
                    else
                    {
                        throw new AuthenticationException("Failed to authorize!");
                    }
                }

                return _isDailyClaimedCache;
            }
        }

        public MonthlyRewardsData MonthlyRewards
        {
            get
            {
                if (_monthlyRewards.Month != DateTime.Now.Month)
                {
                    if (TryGetMothlyRewards(_cookies) == false)
                    {
                        throw new AuthenticationException("Failed to authorize!");
                    }
                }

                return _monthlyRewards;
            }
        }

        public bool TryClaimRewards(out MonthlyRewardData? reward)
        {
            if (IsDailyClaimed)
            {
                reward = null;
                return true;
            }

            HttpRequest request = GenerateMihoyoRequest(_cookies);
            request.Post(Program.ClaimRewardUrl);
            var response = JsonConvert.DeserializeObject<MihoyoResponse>(request.Response.ToString());

            if (response.RetCode == 0)
            {
                if (TryGetCurrentRewardsStatus(_cookies, out RewardsData? rewardStatus) == false)
                {
                    throw new AuthenticationException("Failed to authorize!");
                }

                reward = MonthlyRewards.Awards[rewardStatus.CurrentSignDay - 1];

                return true;
            }

            if (response.RetCode == -100)
            {
                Console.WriteLine("Cookies is expired or invalid!");
                reward = null;

                return false;
            }

            if (response.RetCode == -5003)
            {
                Console.WriteLine("It looks like the reward has been already received, but somehow the request for claim was sent again...");
                reward = null;

                return false;
            }

            Console.WriteLine("An unexpected error occurred while trying to authorize.\n" +
                              "Server response:\n" + request.Response);
            reward = null;

            return false;
        }

        private bool TryGetGameAccountsInfo(CookieCollection cookies, out IReadOnlyList<GameAccountData>? result)
        {
            HttpRequest request = GenerateMihoyoRequest(cookies);
            request.Get(Program.AccountInfoUrl);
            var response = JsonConvert.DeserializeObject<MihoyoResponse>(request.Response.ToString());

            if (response.RetCode == 0)
            {
                var accountsData = JsonConvert.DeserializeObject<AccountsList>(response.Data.ToString());
                result = accountsData!.Accounts;

                return true;
            }

            if (response.RetCode == -100)
            {
                Console.WriteLine("Cookies is expired or invalid!");
                result = null;

                return false;
            }

            Console.WriteLine("An unexpected error occurred while trying to authorize.\n" +
                              "Server response:\n" + request.Response);
            result = null;

            return false;
        }

        private bool TryGetCurrentRewardsStatus(CookieCollection cookies, out RewardsData? rewardStatus)
        {
            HttpRequest request = GenerateMihoyoRequest(cookies);
            request.Get(Program.RewardInfoUrl);
            var response = JsonConvert.DeserializeObject<MihoyoResponse>(request.Response.ToString());

            if (response.RetCode == 0)
            {
#if DEBUG
                Console.WriteLine("Received Mihoyo response to reward info:\n" +
                                  $"{response.Data}");
#endif

                rewardStatus = JsonConvert.DeserializeObject<RewardsData>(response.Data.ToString());

                return true;
            }
            
            if (response.RetCode == -100)
            {
                Console.WriteLine("Cookies is expired or invalid!");
                rewardStatus = null;

                return false;
            }

            Console.WriteLine("An unexpected error occurred while trying to authorize.\n" +
                              "Server response:\n" + request.Response);
            rewardStatus = null;

            return false;
        }

        private bool TryGetMothlyRewards(CookieCollection cookies)
        {
            HttpRequest request = GenerateMihoyoRequest(cookies);
            request.Get(Program.MonthRewardsUrl);
            var response = JsonConvert.DeserializeObject<MihoyoResponse>(request.Response.ToString());

            if (response.RetCode == 0)
            {
                _monthlyRewards = JsonConvert.DeserializeObject<MonthlyRewardsData>(response.Data.ToString());
                return true;
            }

            if (response.RetCode == -100)
            {
                Console.WriteLine("Cookies is expired or invalid!");
                
                return false;
            }

            Console.WriteLine("An unexpected error occurred while trying to authorize.\n" +
                              "Server response:\n" + request.Response);
            

            return false;
        }

        private HttpRequest GenerateMihoyoRequest(CookieCollection cookies)
        {
            var httpRequest = new HttpRequest();

            httpRequest.UserAgent = _selectedUserAgent;

            httpRequest.AddHeader("Accept", "*/*");
            httpRequest.AddHeader("Host", "hk4e-api-os.mihoyo.com");
            httpRequest.AddHeader("Accept-Encoding", "br");
            httpRequest.AddHeader("Connection", "keep-alive");

            httpRequest.Cookies = new CookieStorage();
            httpRequest.Cookies.Set(cookies);

            return httpRequest;
        }
    }
}
