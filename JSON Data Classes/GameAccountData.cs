﻿using Newtonsoft.Json;

namespace AutoDailies
{
    internal class GameAccountData
    {
        [JsonProperty("region_name")]
        public string Region { get; init; }

        [JsonProperty("nickname")]
        public string Nickname { get; init; }

        [JsonProperty("level")]
        public int Level { get; init; }

        public override string ToString()
        {
            return $"{Nickname} [{Level} level] - {Region}";
        }
    }

    internal class AccountsList
    {
        [JsonProperty("list")]
        public IReadOnlyList<GameAccountData> Accounts { get; init; }
    }
}
