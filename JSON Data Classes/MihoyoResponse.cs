﻿using Newtonsoft.Json;

namespace AutoDailies
{
    internal class MihoyoResponse
    {
        [JsonProperty("retcode")]
        public int RetCode { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
