﻿using Newtonsoft.Json;

namespace AutoDailies
{
    internal class MonthlyRewardsData
    {
        [JsonProperty("month")]
        public int Month { get; init; }

        [JsonProperty("awards")]
        public IReadOnlyList<MonthlyRewardData> Awards { get; init; }

        public override string ToString()
        {
            return $"Rewards month: {Month}\n" +
                   $"Awards count: {Awards.Count}";
        }
    }

    internal class MonthlyRewardData
    {
        [JsonProperty("name")]
        public string RewardName { get; init; }

        [JsonProperty("cnt")]
        public int RewardAmount { get; init; }

        public override string ToString()
        {
            return $"{RewardAmount} {RewardName}";
        }
    }
}
