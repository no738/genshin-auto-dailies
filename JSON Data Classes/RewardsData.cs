﻿using Newtonsoft.Json;

namespace AutoDailies
{
    internal class RewardsData
    {
        [JsonProperty("is_sign")]
        public bool IsClaimed { get; init; }

        [JsonProperty("total_sign_day")]
        public int CurrentSignDay { get; init; }

        public override string ToString()
        {
            return $"Reward already claimed: {IsClaimed}\n" +
                   $"Current sign days number: {CurrentSignDay}";
        }
    }

    internal class RewardsList
    {
        [JsonProperty("list")]
        public IReadOnlyList<RewardsData> RewardsData { get; init; }
    }
}